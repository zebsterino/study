$(document).ready(function () {
    $('#knopka').click(function () {
        let heyo = $('#yo').html();
        if (heyo == 'ПРИВЕТ') {
            $('#yo').html('ПОКА');
        }
        if (heyo == 'ПОКА') {
            $('#yo').html('ПРИВЕТ');
        }
    })
})

// let company = prompt('Какая компания создала JavaScript?', '2019');


// // // Без JQuery - Native

// function clickKnopka (){
//     let eblya = document.getElementById('pizda').innerHTML;
//     document.getElementById('pizda').innerHTML='ЛЕЖАТЬ';
//     if (eblya == 'СОСАТЬ'){
//         document.getElementById('pizda').innerHTML='ЛЕЖАТЬ';
//     }
//     if (eblya == 'ЛЕЖАТЬ'){
//         document.getElementById('pizda').innerHTML='СОСАТЬ';
//     }
// }


// const COLOR_RED = "#F00";
// const COLOR_GREEN = "#0F0";
// const COLOR_BLUE = "#00F";
// const COLOR_ORANGE = "#FF7F00";


// let name = 'Иван';

// alert (`Привет, ${name}!`)


// let value = true;
// alert(typeof value);

// value = String(value);
// alert(typeof value);


// let str = "123";
// alert(typeof str); 

// let num = Number(str); 
// alert(typeof num);


// alert( Number("123") ); // 123
// alert( Number("123z") );      // NaN (ошибка чтения числа в "z")
// alert( Number(true) );        // 1
// alert( Number(false) );       // 0


// alert( 1 + '2' ); // '12' (строка справа)
// alert( '1' + 2 ); // '12' (строка слева)

// alert( Boolean(1) ); // true
// alert( Boolean(0) ); // false

// alert( Boolean("Привет!") ); // true
// alert( Boolean("") ); // false

// alert( Boolean("0") ); // true
// alert( Boolean(" ") ); // пробел это тоже true (любая непустая строка это true)

// alert( 5 % 2 ); // 1, остаток от деления 5 на 2

// alert( 2 ** 2 ); // 4  (2 * 2)  - Остаток

// alert( 4 ** (1/2) ); // 2 (степень 1/2 эквивалентна взятию квадратного корня) - Возведение в Степень

// let counter = 2;  // ИНКРЕМЕНТ!!
// counter++;        // работает как counter = counter + 1, просто запись короче
// alert( counter ); // 3

// let counter = 2; // ДЕКРЕМЕНТ!!
// counter--;        // работает как counter = counter - 1, просто запись короче
// alert( counter ); // 1

// !! Инкремент/декремент можно применить только к переменной. Попытка использовать его на значении, типа 5++, вернёт ошибку. !! //


// let counter = 1;  - ПРЕФИКСНАЯ форма
// let a = ++counter; // (*)

// alert(a); // 2


// let counter = 1; - ПОСТФИКСНАЯ форма
// let a = counter++; // (*) меняем ++counter на counter++

// alert(a); // 1


// let a = (1 + 2, 3 + 4); // - ЗАПЯТАЯ

// alert( a ); // 7 (результат 3 + 4)

// let result = 5 > 4; // результат сравнения присваивается переменной result
// alert( result ); // true

// МОДАЛЬНЫЕ ОКНА !! 

// let age = prompt('Сколько тебе лет?', '');  // Функция ПРОМТ - Ввести чет

// alert(`Тебе ${age} лет!`); 

// let isBoss = confirm("Ты здесь главный?"); // Функция КОНФИРМ  - Да или Нет

// alert( isBoss ); // true если нажата OK

// УСЛОВНЫЕ ОПЕРАТОРЫ //

// let year = prompt('В каком году появилась спецификация ECMAScript-2015?', '');
// if (year < 2015) {
//     alert ( 'Дядя, ты шо ку-ку?');
// } else if (year > 2015) {
//     alert ('Ёпта, ну голову хоть включи!');
// } else {
//     alert ('Красава, правильно!');
// } 

// ОПЕРАТОР ? //

// let age = prompt('Возраст?', '');

// let message = (age < 3) ? 'Здравствуй, малыш!' :
//   (age < 18) ? 'Привет!' :
//   (age < 100) ? 'Здравствуйте!' :
//   'Какой необычный возраст!';
// alert( message );
// _________________________________

// let result;

// if (a + b < 4) {
//   result = 'Мало';
// } else {
//   result = 'Много';
// }

// // И вот тоже самое через ? //

// result = (a + b < 4) ? 'Мало' : 'Много';

//__ОПЕРАТОР ИЛИ || _________________//

// let hour = 12;
// let isWeekend = true;

// if (hour < 10 || hour > 18 || isWeekend) {
//   alert( 'Офис закрыт.' ); // это выходной
// }

// +++ //
// alert( 1 || 0 ); // 1
// alert( true || 'no matter what' ); // true

// alert( null || 1 ); // 1 (первое истинное значение)
// alert( null || 0 || 1 ); // 1 (первое истинное значение)
// alert( undefined || null || 0 ); // 0 (поскольку все ложно, возвращается последнее значение)

// +++ //

// let currentUser = null;
// let defaultUser = "John";

// let name = currentUser || defaultUser || "unnamed";

// alert( name ); // выбирается "John" – первое истинное значение

// ____ОПЕРАТОР && (И) ____//

// let hour = 12;
// let minute = 30;

// if (hour == 12 && minute == 30) {
//   alert( 'The time is 12:30' );
// }

// _________ //

// let x = 1;

// if (x > 0) {
//   alert( 'Greater than zero!' );
// }

// ______ Оператор НЕ ! ____ //

// alert( !true ); // false
// alert( !0 ); // true

// alert( !!"non-empty string" ); // true
// alert( !!null ); // false

// ........ЦИКЛЫ WHILE И FOR........ //

// let i = 0;
// while (i < 3) {
//   alert( i );
//   i++;
// }

// Здесь будет 3 ИТЕРАЦИИ - 0,1,2 //

// Еще один пример //

// let i = 3;
// while (i) { // когда i будет равно 0, условие станет ложным, и цикл остановится
//   alert( i );
//   i--;
// }

// Одна строка не требует Фигурные скобки - ПРИМЕР: //

// let i = 3;
// while (i) alert(i--);

// do..while.. //

// let i = 0;
// do {
//   alert( i );
//   i++;
// } while (i < 3);

// ЦИКЛ FOR !!!! //

// for (let i = 0; i < 3; i++) { // выведет 0, затем 1, затем 2
//     alert(i);
//   }

// Переменная СНАРУЖИ ЦИКЛА //

// let i = 0;

// for (i = 0; i < 3; i++) { // используем существующую переменную
//   alert(i); // 0, 1, 2
// }

// alert(i); // 3, переменная доступна, т.к. была объявлена снаружи цикла

// Сокращение Цикла //

// let i = 0;

// for (; i < 3;) {
//   alert( i++ );
// }

// ПРЕРВАТЬ ЦИКЛ - break //

// Переход к след. итерации - CONTINUE //

// for (let i = 0; i < 10; i++) {

//     // если true, пропустить оставшуюся часть тела цикла
//     if (i % 2 == 0) continue;
  
//     alert(i); // 1, затем 3, 5, 7, 9
//   }

  // Т.е Делить на 2 без остатка //

  //  OUTER //

//   outer: for (let i = 0; i < 3; i++) {

//     for (let j = 0; j < 3; j++) {
  
//       let input = prompt(`Значение на координатах (${i},${j})`, '');
  
//       // если пустая строка или Отмена, то выйти из обоих циклов
//       if (!input) break outer; // (*)
  
//       // сделать что-нибудь со значениями...
//     }
//   }
  
//   alert('Готово!');


// КОНСТРУКЦИИ SWITCH //

// let a = 2 + 2;

// switch (a) {
//   case 3:
//     alert( 'Маловато' );
//     break;
//   case 4:
//     alert( 'В точку!' );
//     break;
//   case 5:
//     alert( 'Перебор' );
//     break;
//   default:
//     alert( "Нет таких значений" );
// }

// ЕЩЕ 1 пример SWITCH //

// let a = "1";
// let b = 0;

// switch (+a) {
//   case b + 1:
//     alert("Выполнится, т.к. значением +a будет 1, что в точности равно b+1");
//     break;

//   default:
//     alert("Это не выполнится");
// }

// ГРУППИРОВКА CASE //

// let a = 2 + 2;

// switch (a) {
//   case 4:
//     alert('Правильно!');
//     break;

//   case 3: // (*) группируем оба case
//   case 5:
//     alert('Неправильно!');
//     alert("Может вам посетить урок математики?");
//     break;

//   default:
//     alert('Результат выглядит странновато. Честно.');
// }

// !!! ФУНКЦИИ !!! // 

// function showMessage() {
//     alert( 'Всем привет!' );
//   }
  
//   showMessage();
//   showMessage(); // Дважды увидим сообщение!

// ЛОКАЛЬНЫЕ ПЕРЕМЕННЫЕ //  внутри функции //

// function showMessage() {
//     let message = "Привет, я JavaScript!"; // локальная переменная
  
//     alert( message );
//   }
  
//   showMessage(); // Привет, я JavaScript!
  
//   alert( message ); // <-- будет ошибка, т.к. переменная видна только внутри функции


// ВНЕШНИЕ ПЕРЕМЕННЫЕ // снаружи функции // ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ!!

// let userName = 'Вася';

// function showMessage() {
//   let message = 'Привет, ' + userName;
//   alert(message);
// }

// showMessage(); // Привет, Вася

// Глобальные переменные видимы для любой функции (если только их не перекрывают одноимённые локальные переменные). //

// function showMessage(from, text) {

//     from = '*' + from + '*'; // немного украсим "from"
  
//     alert( from + ': ' + text );
//   }
  
//   let from = "Аня";
  
//   showMessage(from, "Привет"); // *Аня*: Привет
  
//   // значение "from" осталось прежним, функция изменила значение локальной переменной
//   alert( from ); // Аня

// Возврат значений //

// function sum(a, b) {
//     return a + b;
//   }
  
//   let result = sum(1, 2);
//   alert( result ); // 3

// пример //

// function checkAge(age) {
//     if (age > 18) {
//       return true;
//     } else {
//       return confirm('А родители разрешили?');
//     }
//   }
  
//   let age = prompt('Сколько вам лет?', 18);
  
//   if ( checkAge(age) ) {
//     alert( 'Доступ получен' );
//   } else {
//     alert( 'Доступ закрыт' );
//   }

// ПРимер 2 //

// function showPrimes(n) {

//     for (let i = 2; i < n; i++) {
//       if (!isPrime(i)) continue;
  
//       alert(i);  // простое
//     }
//   }
  
//   function isPrime(n) {
//     for (let i = 2; i < n; i++) {
//       if ( n % i == 0) return false;
//     }
//     return true;
//   }

// Function Expression и функции-стрелки //

// Function Expression //

// let sayHi = function() {
//     alert( "Привет" );
//   };

// Мы можем скопировать функцию в другую переменную: //

// function sayHi() {   // (1) создаём
//     alert( "Привет" );
//   }
  
//   let func = sayHi;    // (2) копируем
  
//   func(); // Привет    // (3) вызываем копию (работает)!
//   sayHi(); // Привет   //     прежняя тоже работает (почему бы нет)

// Функции КолБэки // Аргументы Функции //

// function ask(question, yes, no) {
//     if (confirm(question)) yes()
//     else no();
//   }
  
//   function showOk() {
//     alert( "Вы согласны." );
//   }
  
//   function showCancel() {
//     alert( "Вы отменили выполнение." );
//   }
  
//   // использование: функции showOk, showCancel передаются в качестве аргументов ask
//   ask("Вы согласны?", showOk, showCancel);

// КОЛ Бек через Экспрешен Функцию //

// function ask(question, yes, no) {
//     if (confirm(question)) yes()
//     else no();
//   }
  
//   ask(
//     "Вы согласны?",
//     function() { alert("Вы согласились."); },
//     function() { alert("Вы отменили выполнение."); }
//   );

// Еще одна Fun Dec //

// sayHi("Вася"); // Привет, Вася

// function sayHi(name) {
//   alert( `Привет, ${name}` );
// }

// Экспрешен через условный оператор ? //

// let age = prompt("Сколько Вам лет?", 18);

// let welcome = (age < 18) ?
//   function() { alert("Привет!"); } :
//   function() { alert("Здравствуйте!"); };

// welcome(); // теперь всё в порядке


// Функции Стрелки - Arrow // 

// let func = (arg1, arg2, ...argN) => expression ;

// еще пример , если один арумент //

// let double = n => n * 2;

// alert( double(3) ); 

// без аргументов //

// let sayHi = () => alert("Hello!");

// sayHi();

// Ф.Стрелки в Экспрешен //

// let age = prompt("Сколько Вам лет?", 18);

// let welcome = (age < 18) ?
//   () => alert('Привет') :
//   () => alert("Здравствуйте!");

// welcome(); 


// еще пример //

// let sum = (a, b) => {  // фигурная скобка, открывающая тело многострочной функции
//     let result = a + b;
//     return result; // при фигурных скобках нужен return, чтобы вернуть результат
//   };
  
//   alert( sum(1, 2) ); // 3

/// ОБЪЕКТЫ ///

// let user = new Object(); // синтаксис "конструктор объекта"
// let user = {};  // синтаксис "литерал объекта"

// let user = {     // объект
//     name: "John",  // под ключом "name" хранится значение "John"
//     age: 30        // под ключом "age" хранится значение 30
//   };

// Обращение через ТОЧКУ //
// alert( user.name ); // John
// alert( user.age ); // 30

// let user = {
//     name: "John",
//     age: 30,
//     "likes birds": true  // имя свойства из нескольких слов должно быть в кавычках
//   };


// let user = {
//     name: "John",
//     age: 30
//   };
  
//   let key = prompt("Что вы хотите узнать о пользователе?", "name");
  
//   // доступ к свойству через переменную
//   alert( user[key] ); // John (если ввели "name")


// let fruit = prompt("Какой фрукт купить?", "apple");

// let bag = {
//   [fruit]: 5, // имя свойства будет взято из переменной fruit
// };

// alert( bag.apple ); // 5, если fruit="apple"


// function makeUser(name, age) {
//     return {
//       name: name,
//       age: age
//       // ...другие свойства
//     };
//   }
  
//   let user = makeUser("John", 30);
//   alert(user.name); // John


// let user = {};
// alert( user.noSuchProperty === undefined ); // true означает "свойства нет"


// let user = { name: "John", age: 30 };

// alert( "age" in user ); // true, user.age существует
// alert( "blabla" in user ); // false, user.blabla не существует


// let user = {
//     name: "John",
//     age: 30,
//     isAdmin: true
//   };
  
//   for (let key in user) {
//     // ключи
//     alert( key );  // name, age, isAdmin
//     // значения ключей
//     alert( user[key] ); // John, 30, true
//   }


// let codes = {
//     "49": "Германия",
//     "41": "Швейцария",
//     "44": "Великобритания",
//     // ..,
//     "1": "США"
//   };
  
//   for (let code in codes) {
//     alert(code); // 1, 41, 44, 49
//   } --------- В поряде позврастания все будет показываться

/// А тут нет! //

// let codes = {
//     "+49": "Германия",
//     "+41": "Швейцария",
//     "+44": "Великобритания",
//     // ..,
//     "+1": "США"
//   };
  
//   for (let code in codes) {
//     alert( +code ); // 49, 41, 44, 1
//   }


// let user = { name: 'John' };

// let admin = user;

// admin.name = 'Pete'; // изменено по ссылке из переменной "admin"

// alert(user.name); // 'Pete', изменения видны по ссылке из переменной "user"


// let a = {};
// let b = a; // копирование по ссылке

// alert( a == b ); // true, обе переменные ссылаются на один и тот же объект
// alert( a === b ); // true


// let user = {
//     name: "John",
//     age: 30
//   };
  
//   let clone = {}; // новый пустой объект
  
//   // скопируем все свойства user в него
//   for (let key in user) {
//     clone[key] = user[key];
//   }
  
//   // теперь в переменной clone находится абсолютно независимый клон объекта.
//   clone.name = "Pete"; // изменим в нём данные
  
//   alert( user.name ); // в оригинальном объекте значение свойства `name` осталось прежним – John.

// let user = {
//     name: "John",
//     age: 30
//   };
  
//   let clone = Object.assign({}, user);


// let user = {
//     name: "John",
//     sizes: {
//       height: 182,
//       width: 50
//     }
//   };
  
//   alert( user.sizes.height ); // 182


// let user = {
//     name: "John",
//     sizes: {
//       height: 182,
//       width: 50
//     }
//   };
  
//   let clone = Object.assign({}, user);
  
//   alert( user.sizes === clone.sizes ); // true, один и тот же объект
  
//   // user и clone обращаются к одному sizes
//   user.sizes.width++;       // меняем свойство в одном объекте
//   alert(clone.sizes.width); // 51, видим результат в другом объекте


// Концепция ДОСТИЖИМОСТИ //

// function marry(man, woman) {
//     woman.husband = man;
//     man.wife = woman;
  
//     return {
//       father: man,
//       mother: woman
//     }
//   }
  
//   let family = marry({
//     name: "John"
//   }, {
//     name: "Ann"
//   });


// СИМВОЛЫ //

// let id1 = Symbol("id");
// let id2 = Symbol("id");

// alert(id1 == id2); // false


// let id = Symbol("id");
// alert(id.toString()); // Symbol(id), теперь работает


// let id = Symbol("id");
// let user = {
//   name: "Вася",
//   age: 30,
//   [id]: 123
// };

// for (let key in user) alert(key); // name, age (свойства с ключом-символом нет среди перечисленных)

// // хотя прямой доступ по символу работает
// alert( "Напрямую: " + user[id] );


// let id = Symbol("id");
// let user = {
//   [id]: 123
// };

// let clone = Object.assign({}, user);

// alert( clone[id] ); // 123



// // читаем символ из глобального реестра и записываем его в переменную
// let id = Symbol.for("id"); // если символа не существует, он будет создан

// // читаем его снова в другую переменную (возможно, из другого места кода)
// let idAgain = Symbol.for("id");

// // проверяем -- это один и тот же символ
// alert( id === idAgain ); // true


// // получаем символ по имени
// let sym = Symbol.for("name");
// let sym2 = Symbol.for("id");

// // получаем имя по символу
// alert( Symbol.keyFor(sym) ); // name
// alert( Symbol.keyFor(sym2) ); // id


// let globalSymbol = Symbol.for("name");
// let localSymbol = Symbol("name");

// alert( Symbol.keyFor(globalSymbol) ); // name, глобальный символ
// alert( Symbol.keyFor(localSymbol) ); // undefined для неглобального символа

// alert( localSymbol.description ); // name


// Symbol.hasInstance
// Symbol.isConcatSpreadable
// Symbol.iterator
// Symbol.toPrimitive
// …и так далее.


// ПРИМЕР МЕТОДОВ //

// let user = {
//     name: "Джон",
//     age: 30
//   };
  
//   user.sayHi = function() {
//     alert("Привет!");
//   };
  
//   user.sayHi(); 


// СОКРАЩЕННАЯ ФОРМА //

// user = {
//     sayHi() {
//       alert("Привет");
//     }
//   };

// THIS в МЕТОДАХ -  ТЕКУЩИЙ ОБЪЕКТ//

// let user = {
//     name: "Джон",
//     age: 30,
  
//     sayHi() {
//       alert(this.name);
//     }
  
//   };
  
//   user.sayHi();


// ПРЕОБРАЗОВАНИЕ К ПРИМИТИВАМ

// STRING 

// alert(obj);
// anotherObj[obj] = 123;

// NUMBER

// let num = Number(obj);

// +

// let n = +obj;
// let delta = date1 - date2;

// больше/меньше

// let greater = user1 > user2;


// // бинарный плюс
// let total = car1 + car2;

// // obj == string/number/symbol
// if (user == 1) { ... };


// Вызывает obj[Symbol.toPrimitive](hint) – метод с символьным ключом Symbol.toPrimitive (системный символ), если такой метод существует, и передаёт ему хинт.
// Иначе, если хинт равен "string"
// пытается вызвать obj.toString(), а если его нет, то obj.valueOf(), если он существует.
// В случае, если хинт равен "number" или "default"
// пытается вызвать obj.valueOf(), а если его нет, то obj.toString(), если он существует.


// obj[Symbol.toPrimitive] = function(hint) {
//     // должен вернуть примитивное значение
//     // hint равно чему-то одному из: "string", "number" или "default"
//   };

// let user = {
//     name: "John",
//     money: 1000,
  
//     [Symbol.toPrimitive](hint) {
//       alert(`hint: ${hint}`);
//       return hint == "string" ? `{name: "${this.name}"}` : this.money;
//     }
//   };
  
//   // демонстрация результатов преобразований:
//   alert(user); // hint: string -> {name: "John"}
//   alert(+user); // hint: number -> 1000
//   alert(user + 500); // hint: default -> 1500


// toString / valueOf //

// let user = {
//     name: "John",
//     money: 1000,
  
//     toString() {
//       return `{name: "${this.name}"}`;
//     },
  
//     valueOf() {
//       return this.money;
//     }
  
//   };
  
//   alert(user); // {name: "John"}
//   alert(+user); // 1000
//   alert(user + 500); // 1500

// еще пример //

// let user = {
//     name: "John",
  
//     toString() {
//       return this.name;
//     }
//   };
  
//   alert(user); // toString -> John
//   alert(user + 500); // toString -> John500

// Умножение //

// let obj = {
//     // toString обрабатывает все преобразования в случае отсутствия других методов
//     toString() {
//       return "2";
//     }
//   };
  
//   alert(obj * 2); // 4

// Сложение //

// let obj = {
//     toString() {
//       return "2";
//     }
//   };
  
//   alert(obj + 2); // 22

