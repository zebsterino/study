const pageSize = 8;
let currentPage = 0;
let itemsCount = 0;
let products = [];
let productsToStore = [];


$(document).ready(function () {
  let items = localStorage.getItem("products");
  if (items != null && items != undefined) {
    productsToStore = JSON.parse(items);
  }
  loadJSON(function (data) {
    let parsed = JSON.parse(data);
    itemsCount = parsed.products.length;
    products = parsed.products;
    currentPage = 1;
    let printItemsCount = parsed.products.length;
    if (itemsCount >= pageSize) {
      printItemsCount = pageSize;
    }
    for (let i = 0; i < printItemsCount; i++) {
      printCartItem(parsed.products[i]);
    }
    printPagination();
    $('.page-link').click(function () {
      $('.card').remove();
      let drowFrom = event.target.computedName * pageSize - pageSize;
      printItemsCount = drowFrom + pageSize;
      if (printItemsCount > itemsCount) {
        printItemsCount = itemsCount;
      }
      for (let i = drowFrom; i < printItemsCount; i++) {
        printCartItem(parsed.products[i]);
      }
      $('.active').removeClass("active");
      $(this).parent().addClass("active");
      $('.buttonBuy').click (function () {
        let id = event.path[2].id;
        let productToLocalStorage;
        for (let i = 0; i < products.length; i++) {
          if (products[i].id == id) {
            productToLocalStorage = products[i];
            break;
          }
        }
        productsToStore.push(productToLocalStorage);
        localStorage.setItem("products", JSON.stringify(productsToStore));
        printCart(productToLocalStorage);
      });
    });
    $('.buttonBuy').click (function () {
      let id = event.path[2].id;
      let productToLocalStorage;
      for (let i = 0; i < products.length; i++) {
        if (products[i].id == id) {
          productToLocalStorage = products[i];
          break;
        }
      }
      productsToStore.push(productToLocalStorage);
      localStorage.setItem("products", JSON.stringify(productsToStore));
      printCart(productToLocalStorage);
    });
  });
});

function printCart(productToLocalStorage) {
  $('.modal-products').append(`<h2>${productToLocalStorage.name} - ${productToLocalStorage.price}`)
}


function loadJSON(callback) {

  let xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', 'https://api.myjson.com/bins/138mpu', true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}


function printCartItem(item) {
  $('#tableMain').append(`
  <div class="card" id="${item.id}" style="width: 18rem;">
    <img src="${item.imgPath}" class="card-img-top" alt="Telescope">
    <div class="card-body">
      <h5 class="card-title">${item.name}</h5>
      <p class="card-text">${item.description}</p>
      <a href="javascript:void(0)" class="btn btn-primary buttonBuy">Buy</a>
      <p style="float: right; padding-right: 75px; padding-top: 5px;"><b>Price:</b>${item.price}</p>
    </div>
  </div>`);
}


function printPagination() {
  let pageCount = itemsCount / pageSize;
  pageCount = Math.ceil(pageCount);
  for (let i = 2; i <= pageCount; i++) {
    $('#pagination').append(`<li class="page-item"><a class="page-link" href="javascript:void(0);">${i}</a></li>`);
  }
}

